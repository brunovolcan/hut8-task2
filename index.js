// Chamar os pacotes
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Post = require('./posts')
const User = require('./user')
require('dotenv').config()
const bcrypt = require('bcryptjs')
// const jwt = require('jsonwebtoken')
// ============================================

// Conectar atlas

mongoose.connect(process.env.MY_ATLAS, {
  useUnifiedTopology: true,
  useNewUrlParser: true

})
// ============================================

// Configuração para o Body-parser
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
// ============================================

// Porta na qual será executado
const port = process.env.PORT || process.env.LOCAL_PORT
// ============================================

// Criando uma instância das Rotas via Express
const router = express.Router()
// ============================================

// Executa logo após uma requisição
router.use((req, res, next) => {
  console.log('Processing...')
  next()
})
// ============================================

// Rota de exemplo
router.get('/', (req, res) => { res.json({ message: 'Working!!!' }) })
// ============================================

// Rotas que terminarem com '/posts' (servir: GET ALL e POST)
router.route('/posts')

  // Cria um post
  .post((req, res) => {
    const post = new Post()

    post.titulo = req.body.titulo
    post.texto = req.body.texto
    post.likes = req.body.likes
    post.autor = req.body.autor

    post.save((error) => {
      if (error) { res.send('Erro: ' + error) }
      res.json({ message: 'Post Publicado!' })
    })
  })
  // ===========================================

  // Mostra todos os posts
  .get((req, res) => {
    Post.find((error, posts) => {
      if (error) { res.send('Erro ao tentar selecionar todos os posts: ' + error) }
      res.json(posts)
    })
  })
  // ========================================

// Rotas pelo id
router.route('/posts/:post_id')
// =============================================

// Selecionar por id (acessar em: GET http://localhost:3000/api/posts/:post_id)
  .get((req, res) => {
    Post.findById(req.params.post_id, (error, post) => {
      if (error) { res.send('Id do Post não encontrada. Erro: ' + error) }
      if (post === null) { res.send('Post não encontrado!') } else { res.json(post) }
    })
  })

// Atualizar por id (acessar em: PUT http://localhost:3000/api/posts/:post_id)
  .put((req, res) => {
    Post.findById(req.params.post_id, (error, post) => {
      if (error) { res.send(' Id do Post não encontrada. Erro: ' + error) }

      post.titulo = req.body.titulo
      post.texto = req.body.texto
      post.likes = req.body.likes
      post.autor = req.body.autor

      post.save((error) => {
        if (error) { res.send('Erro ao atualizar o post. Erro: ' + error) }
        res.json({ message: 'Post atualizado!' })
      })
    })
  })
// ============================================

  .delete((req, res) => {
    Post.findById(req.params.post_id, (error, post) => {
      if (error) { res.send(' Id do post não encontrada. Erro: ' + error) }

      post.deleteOne(post, (error) => {
        if (error) { res.send(' Falha ao deletar o post. Erro: ' + error) }
        res.json({ message: 'Post deletado!' })
      })
    })
  })

// Cria um usuário
router.post('/user', (req, res) => {
  const user = new User()

  user.username = req.body.username
  if (req.body.password) {
    user.password = bcrypt.hashSync(req.body.password, 10)
  }
  user.bio = req.body.bio

  user.save((error) => {
    if (error) { res.send(' Falha ao criar usuário. Erro: ' + error) }
    res.json({ message: 'Usuário criado!' })
  })
})
// ============================================

// Mostra todos usuários
router.get('/user', (req, res) => {
  User.find((error, user) => {
    if (error) { res.send('Erro ao tentar selecionar todos os usuários: ' + error) }
    res.json(user)
  })
})
// ============================================

// Acha usuário por ID
router.get('/user/:user_id', (req, res) => {
  User.findById(req.params.user_id, (error, user) => {
    if (error) { res.send('Id do Usuário não encontrada. Erro: ' + error) }
    if (user === null) { res.send('Usário não encontrado!') } else { res.json(user) }
  })
})
// ============================================

// Atualizar usuario por ID
router.put('/user/:user_id', (req, res) => {
  User.findById(req.params.user_id, (error, user) => {
    if (error) { res.send('Id do Usuário não encontrada. Erro: ' + error) }

    user.username = req.body.username
    if (req.body.password) {
      user.password = bcrypt.hashSync(req.body.password, 10)
    }
    user.bio = req.body.bio

    user.save((error) => {
      if (error) { res.send('Erro ao atualizar o Usuário. Erro: ' + error) }
      res.json({ message: 'Usuário atualizado!' })
    })
  })
})
// ============================================

// Exclui um usuário por ID
router.delete('/user/:user_id', (req, res) => {
  User.findById(req.params.user_id, (error, user) => {
    if (error) { res.send(' Id do Usuário não encontrada. Erro: ' + error) }

    user.deleteOne(user, (error) => {
      if (error) { res.send(' Falha ao deletar o Usuário. Erro: ' + error) }
      res.json({ message: 'Usuário deletado!' })
    })
  })
})
// =============================================

// Login
// router.post('/login', (req, res) => {
//  jwt.sign()
// })

// ============================================

// Definindo um padrão das rotas prefixiadas: '/api'
app.use('/api', router)
// ============================================

// Iniciando
app.listen(port)
console.log('Iniciando na porta ' + port)
// ============================================
