const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PostSchema = new Schema({
  titulo: String,
  texto: String,
  likes: Number,
  autor: String
})

module.exports = mongoose.model('Post', PostSchema)
